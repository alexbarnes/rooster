# Steps to get the project running
You will need to install the NPM dependencies using Npm or Yarn

    yarn install

When you are ready to run the node code locally

    yarn start

AWS Lambda can be simulated locally

    yarn start-sls

To Deploy the project on AWS Lambda
    
    yarn deploy-qa

# Rooster Bank - API

An easy way to view the API docs is to load the [Swagger definition file](./src/config/swagger.json)
 into [https://editor.swagger.io/](https://editor.swagger.io/)
 
## Authentication

The Rooster Bank API is protected using Json Web Tokens [JWT](https://jwt.io/introduction/).

To retrieve a new token use your favourite client for example [Postman](https://www.getpostman.com/) or [cURL](https://curl.haxx.se/)

     curl -X POST "https://api.abarnes.uk/Rooster/auth" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"username\": \"JonSmart\", \"password\": \"Challenge\"}"

This will return the following:
* access_token = The Access Token is a credential that can be used by an application to access an API. The Access Token 
should be used as a Bearer credential and transmitted in an HTTP Authorization header to the API.
* id_token = The ID Token is a JSON Web Token (JWT) that contains user profile information (like the user's name, email, and so forth), represented in the form of claims. These claims are statements about the user, which can be trusted if the consumer of the token can verify its signature.
* expires_in = The number of seconds before the tokens expire.


Example of using the access_token to retrieve the users Parent resource:

    curl -X GET 
      https://api.abarnes.uk/Rooster/parents/100 \
      -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzU4MzAwNjEsImV4cCI6MTUzNTgzMzY2MSwic3ViIjoiMTAwIn0.8V4KYabzFr-60-myjTYPS3NHMjCpqqpRy26TB0zo6oQ'


## Get All Balances
Develop a route that allows an Authenticated user to obtain the balances of ALL their children, this end-point should return all the children's balances.

    curl -X GET \
      https://api.abarnes.uk/Rooster/parents/100/children \
      -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzU4MzAwNjEsImV4cCI6MTUzNTgzMzY2MSwic3ViIjoiMTAwIn0.8V4KYabzFr-60-myjTYPS3NHMjCpqqpRy26TB0zo6oQ'

Example output

```json
{
    "_links": {
        "self": {
            "href": "/Rooster/parents/100/children"
        }
    },
    "_embedded": {
        "children": [
            {
                "_links": {
                    "self": {
                        "href": "/Rooster/parents/100/children/301"
                    },
                    "parent": {
                        "href": "/Rooster/parents/100"
                    }
                },
                "id": 301,
                "firstName": "Kimi",
                "lastName": "Machen",
                "gender": "female",
                "age": 8,
                "pocketMoneyAmount": 200,
                "currency": "gbp",
                "balances": {
                    "spend": 2307,
                    "save": 1000,
                    "give": 0
                }
            },
            {
                "_links": {
                    "self": {
                        "href": "/Rooster/parents/100/children/303"
                    },
                    "parent": {
                        "href": "/Rooster/parents/100"
                    }
                },
                "id": 303,
                "firstName": "Eugena",
                "lastName": "Portalatin",
                "gender": "female",
                "age": 10,
                "pocketMoneyAmount": 100,
                "currency": "gbp",
                "balances": {
                    "spend": 2384,
                    "save": 1000,
                    "give": 0
                }
            },
            {
                "_links": {
                    "self": {
                        "href": "/Rooster/parents/100/children/304"
                    },
                    "parent": {
                        "href": "/Rooster/parents/100"
                    }
                },
                "id": 304,
                "firstName": "Greg",
                "lastName": "Gabbert",
                "gender": "male",
                "age": 8,
                "pocketMoneyAmount": 200,
                "currency": "gbp",
                "balances": {
                    "spend": 2307,
                    "save": 1000,
                    "give": 0
                }
            }
        ]
    },
    "start": 0,
    "count": 3,
    "total": 3
}
```

## Share Money
Create end point that takes an amount to be shared with all the children associated with the Authenticated user and distribute the amount between them based on the ratio of their age.  All money should be added to their Spend total.

    curl -X POST \
      https://api.abarnes.uk/Rooster/parents/100/share \
      -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzU4MzAwNjEsImV4cCI6MTUzNTgzMzY2MSwic3ViIjoiMTAwIn0.8V4KYabzFr-60-myjTYPS3NHMjCpqqpRy26TB0zo6oQ' \
      -H 'Content-Type: application/json' \
      -d '{
    	"amount": 1000
    }'

## Contact
Any questions please feel free to reach out to me

