import { Operation } from 'express-openapi';
import { UNAUTHORIZED, OK } from 'http-status-codes';
import { AuthService } from '../libs/Auth/AuthService';
import { TokenGenerator } from '../libs/Auth/TokenGenerator';

const TOKEN_EXPIRY = 3600;
const SECRET = 'shhhh'; // TODO: refactor to external config

export default function(authService: AuthService, generator: TokenGenerator) {
  const post: Operation = [
    async (req, res) => {
      const authUser = await authService
        .authenticate(req.body.username, req.body.password)
        .catch(err => {
          throw { status: UNAUTHORIZED, message: 'Authentication Failed' };
        });

      // TODO: refactor to AuthTokenDTO
      const token = {
        access_token: generator.createAccessToken(
          `${authUser.id}`,
          SECRET,
          TOKEN_EXPIRY,
        ),
        id_token: generator.createIdToken(authUser, SECRET, TOKEN_EXPIRY),
        expires_in: TOKEN_EXPIRY,
      };

      res.status(OK).send(token);
    },
  ];

  return {
    post,
  };
}
