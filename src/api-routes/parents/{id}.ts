import { Operation } from 'express-openapi';
import { NOT_FOUND, OK } from 'http-status-codes';
import { ParentService } from '../../libs/Parent/ParentService';

export default function(parentService: ParentService) {
  const get: Operation = [
    async (req, res) => {
      const parent = await parentService
        .getParent(parseInt(req.user.sub, 10), parseInt(req.params.id, 10))
        .catch(err => {
          throw { status: NOT_FOUND, message: err };
        });

      res.status(OK).send(parent.toHal());
    },
  ];

  return {
    get,
  };
}
