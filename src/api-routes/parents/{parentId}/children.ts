import { Operation } from 'express-openapi';
import { NOT_FOUND, OK } from 'http-status-codes';
import { ParentService } from '../../../libs/Parent/ParentService';
import { ChildCollection } from '../../../libs/Child/ChildCollection';

export default function(parentService: ParentService) {
  const get: Operation = [
    async (req, res) => {
      const children = await parentService
        .getChildren(
          parseInt(req.user.sub, 10),
          parseInt(req.params.parentId, 10),
        )
        .catch(err => {
          throw { status: NOT_FOUND, message: err };
        });

      const collection = new ChildCollection(children, req.params.parentId);

      res.status(OK).json(collection.toHal());
    },
  ];

  return {
    get,
  };
}
