import { Operation } from 'express-openapi';
import { NOT_FOUND, OK } from 'http-status-codes';
import { ChildService } from '../../../../libs/Child/ChildService';

export default function(childService: ChildService) {
  const get: Operation = [
    async (req, res) => {
      const child = await childService
        .getChild(parseInt(req.user.sub, 10), parseInt(req.params.childId, 10))
        .catch(err => {
          throw { status: NOT_FOUND, message: err };
        });

      res.status(OK).send(child.toHal());
    },
  ];

  return {
    get,
  };
}
