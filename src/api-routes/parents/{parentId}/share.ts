import { Operation } from 'express-openapi';
import { NO_CONTENT, NOT_FOUND } from 'http-status-codes';
import { ParentService } from '../../../libs/Parent/ParentService';

export default function(parentService: ParentService) {
  const post: Operation = [
    async (req, res) => {
      await parentService
        .shareMoney(
          parseInt(req.user.sub, 10),
          parseInt(req.params.parentId, 10),
          req.body.amount,
        )
        .catch(err => {
          throw { status: NOT_FOUND, message: err };
        });

      res.status(NO_CONTENT).end();
    },
  ];

  return {
    post,
  };
}
