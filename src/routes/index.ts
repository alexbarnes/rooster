import * as express from 'express';
// import roosterMoneyDataAccess from '../data';
import { OK } from 'http-status-codes';
const indexRouter: express.Router = express.Router();

indexRouter.get('', (req: express.Request, res: express.Response) =>
  res.status(OK).end(),
);

export default indexRouter;
