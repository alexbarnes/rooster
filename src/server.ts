import * as dotenv from 'dotenv';
dotenv.config({ path: '../.env' });

import { AddressInfo } from 'net';
import app from './express';
import env from './env';

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection at:', p, 'reason:', reason);
  // send entire app down. Process manager will restart it
  process.exit(1);
});

// start server & listen
const server = app.listen(app.get('port'), () => {
  const info = <AddressInfo>server.address();
  console.info(
    `Express server listening on port ${info.port} in ${env.nodeEnv} mode.`,
  );
});

export default server;
