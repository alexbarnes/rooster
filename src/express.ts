import * as express from 'express';
import * as openapi from 'express-openapi';
import * as jwt from 'express-jwt';
import * as fs from 'fs';
import * as path from 'path';
import * as filter from 'content-filter';
import env from './env';
import * as helmet from 'helmet';
import routes from './routes';
import RoosterMoneyDataAccess from './data';
import { ParentAuthService } from './libs/Auth/AuthService';
import { JwtGenerator } from './libs/Auth/TokenGenerator';
import { ParentMockDataService } from './libs/Parent/ParentService';
import { ChildMockDataService } from './libs/Child/ChildService';

function configureSecurityMiddleware(app: express.Express): void {
  // helmet with default settings
  app.use(helmet());
  // filter malicious code and scripts from body or url
  const blackList: string[] = [
    '$or',
    '$and',
    '$not',
    '$nor',
    '$eq',
    '$gt',
    '$gte',
    '$lt',
    '$lte',
    '$ne',
    '$in',
    '$nin',
    '$exists',
    '$type',
    '$mod',
    '$regex',
    '$text',
    '$where',
    '$all',
    '$elemMatch',
    '&&',
    '||',
    'function()',
    '<script>',
    '</script>',
    '<script',
  ];
  const options = {
    urlBlackList: blackList,
    bodyBlackList: blackList,
    methodList: ['GET', 'POST', 'PUT', 'DELETE'],
    dispatchToErrorHandler: true,
  };
  app.use(filter(options));
}

// export function start():Express.Application {
const app: express.Express = express();
// secure the server
configureSecurityMiddleware(app);
// configure json & urlencoded body parsers
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//
// Authentication
// -----------------------------------------------------------------------------
// TODO: refactor secret to external config or preferrably JWKS endpoint
app.use(
  jwt({ secret: 'shhhh' }).unless({
    path: ['/', '/Rooster/api-docs', '/Rooster/auth', '/Rooster/auth.ts'],
  }),
);

// configure the routes
app.use('/', routes);

/* Typescript is transpiled by mocha on the fly so need to config
  express-openapi differently */
let routesGlob = '**/*.js';
if (process.env.NODE_ENV === undefined) {
  routesGlob = '**/*.ts';
}

const errHandler: express.ErrorRequestHandler = (err, req, res, next) => {
  console.error(err);
  res.status(err.status || 500).json(err);
};

//
// Express Open-API
// -----------------------------------------------------------------------------
const childService = new ChildMockDataService(RoosterMoneyDataAccess);

openapi.initialize({
  app,
  routesGlob,
  apiDoc: fs.readFileSync(
    path.resolve(__dirname, './config/swagger.yaml'),
    'utf8',
  ),
  // TODO: refactor dependency creation to factories (lazy-load?)
  dependencies: {
    childService,
    authService: new ParentAuthService(RoosterMoneyDataAccess),
    generator: new JwtGenerator(),
    parentService: new ParentMockDataService(
      RoosterMoneyDataAccess,
      childService,
    ),
  },
  errorMiddleware: errHandler,
  paths: path.resolve(__dirname, 'api-routes'),
  promiseMode: true,
});

//
// Error Handler
// -----------------------------------------------------------------------------
app.use(errHandler);

// set the port and create the web server
app.set('port', env.port || 3000);

export default app;
