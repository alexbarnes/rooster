export interface ParentData {
  id: number;
  username: string;
  password: string;
  token?: string;
  firstName: string;
  lastName: string;
  children: number[];
}

export interface ChildData {
  id: number;
  firstName: string;
  lastName: string;
  gender: 'male' | 'female';
  age: number;
  pocketMoneyAmount: number;
  currency: string;
  balances: {
    spend: number;
    save: number;
    give: number;
  };
}

const PARENT_TEST_DATA: ParentData[] = [
  {
    id: 100,
    username: 'JonSmart',
    password: 'Challenge',
    firstName: 'Jonathan',
    lastName: 'Smart',
    children: [301, 303, 304],
  },
  {
    id: 101,
    username: 'FredBloggs',
    password: 'Challenge',
    firstName: 'Fred',
    lastName: 'Bloggs',
    children: [],
  },
];

const CHILD_TEST_DATA: ChildData[] = [
  {
    id: 301,
    firstName: 'Kimi',
    lastName: 'Machen',
    gender: 'female',
    age: 8,
    pocketMoneyAmount: 200,
    currency: 'gbp',
    balances: {
      spend: 2000,
      save: 1000,
      give: 0,
    },
  },
  {
    id: 302,
    firstName: 'Muoi',
    lastName: 'Striegel',
    gender: 'female',
    age: 8,
    pocketMoneyAmount: 200,
    currency: 'gbp',
    balances: {
      spend: 2000,
      save: 1000,
      give: 0,
    },
  },
  {
    id: 303,
    firstName: 'Eugena',
    lastName: 'Portalatin',
    gender: 'female',
    age: 10,
    pocketMoneyAmount: 100,
    currency: 'gbp',
    balances: {
      spend: 2000,
      save: 1000,
      give: 0,
    },
  },
  {
    id: 304,
    firstName: 'Greg',
    lastName: 'Gabbert',
    gender: 'male',
    age: 8,
    pocketMoneyAmount: 200,
    currency: 'gbp',
    balances: {
      spend: 2000,
      save: 1000,
      give: 0,
    },
  },
];

export class RoosterMoneyDataAccess {
  public authenticateUser(username: string, password: string): Promise<number> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const parent = PARENT_TEST_DATA.find(
          (parent: ParentData) => parent.username === username,
        );
        if (parent) {
          if (parent.password === password) {
            parent.token = Date.now().toString(16);
            resolve(parent.id);
          }
          reject('Password failed');
        }
        reject('User Not Found');
      }, 100);
    });
  }

  public getUserId(token: string): Promise<number> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const parent = PARENT_TEST_DATA.find(
          (parent: ParentData) => parent.token === token,
        );

        if (parent) {
          resolve(parent.id);
        } else {
          reject('Token Not Found');
        }
      }, 100);
    });
  }

  public getToken(userId: number): Promise<string> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const parent = PARENT_TEST_DATA.find(
          (parent: ParentData) => parent.id === userId,
        );

        if (parent) {
          resolve(parent.token);
        } else {
          reject('UserId Not Found');
        }
      }, 100);
    });
  }

  public getParentData(userId: number): Promise<ParentData> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const parent = PARENT_TEST_DATA.find(
          (parent: ParentData) => parent.id === userId,
        );

        if (parent) {
          resolve(parent);
        } else {
          reject('UserId Not Found');
        }
      }, 100);
    });
  }

  public getChildData(userId: number, childId: number): Promise<ChildData> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const parent = PARENT_TEST_DATA.find(
          (parent: ParentData) => parent.id === userId,
        );

        if (parent) {
          if (childId) {
            const isChild = parent.children.find(id => id === childId);

            if (isChild) {
              const child = CHILD_TEST_DATA.find(child => child.id === childId);

              if (child) {
                resolve(child);
              } else {
                reject('Child Not Found!');
              }
            } else {
              reject("Child doesn't belong to parent");
            }
          } else {
            reject('No ChildID.');
          }
        } else {
          reject('UserId Not Found');
        }
      }, 100);
    });
  }

  public updateChildBalances(
    userId: number,
    childId: number,
    balances: { spend: number; give: number; save: number },
  ) {
    return this.getChildData(userId, childId).then(
      child => (child.balances = balances),
    );
  }
}

export default new RoosterMoneyDataAccess();
