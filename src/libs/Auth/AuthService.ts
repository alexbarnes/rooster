import { ParentData, RoosterMoneyDataAccess } from '../../data';

export interface AuthService {
  authenticate(username: string, password: string): Promise<ParentData>;
}

export class ParentAuthService implements AuthService {
  private readonly dataGateway: RoosterMoneyDataAccess;

  constructor(gateway: RoosterMoneyDataAccess) {
    this.dataGateway = gateway;
  }

  async authenticate(username: string, password: string) {
    const id = await this.dataGateway.authenticateUser(username, password);
    return this.dataGateway.getParentData(id);
  }
}
