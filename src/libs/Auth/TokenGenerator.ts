import * as jwt from 'jsonwebtoken';
import { ParentData } from '../../data';

export interface TokenGenerator {
  createAccessToken(subject: string, secret: string, expiry: number): string;
  createIdToken(parent: ParentData, secret: string, expiry: number): string;
}

export class JwtGenerator implements TokenGenerator {
  createAccessToken(subject: string, secret: string, expiry: number): string {
    return jwt.sign({}, secret, {
      subject,
      expiresIn: expiry,
    });
  }

  createIdToken(parent: ParentData, secret: string, expiry: number): string {
    const cleanToken = { ...parent };
    delete cleanToken.id;
    delete cleanToken.password;
    delete cleanToken.children;

    return jwt.sign(cleanToken, secret, {
      expiresIn: expiry,
      subject: `${parent.id}`,
    });
  }
}
