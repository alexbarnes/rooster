// @ts-ignore
import { HalModel } from '../Hal/HalModel';
import { ChildModel } from './ChildModel';
import { HalCollection } from '../Hal/HalCollection';

export class ChildCollection implements HalModel {
  children: ChildModel[];
  parentId: number;

  constructor(children: ChildModel[], parentId: number) {
    this.children = children;
    this.parentId = parentId;
  }

  toHal() {
    const collection = new HalCollection(
      `/Rooster/parents/${this.parentId}/children`,
      this.children.map(child => child.toResource()),
      0,
      this.children.length,
      'children',
    );

    return collection.toHal();
  }
}
