import { RoosterMoneyDataAccess } from '../../data';
import { ChildModel } from './ChildModel';

export interface ChildService {
  getChild(userId: number, childId: number): Promise<ChildModel>;
}

export class ChildMockDataService implements ChildService {
  private readonly dataGateway: RoosterMoneyDataAccess;

  constructor(gateway: RoosterMoneyDataAccess) {
    this.dataGateway = gateway;
  }

  async getChild(userId: number, childId: number) {
    const data = await this.dataGateway.getChildData(userId, childId);

    return new ChildModel(data, userId);
  }
}
