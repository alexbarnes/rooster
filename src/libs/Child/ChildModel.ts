// @ts-ignore
import * as hal from 'hal';
import { ChildData } from '../../data';
import { HalModel } from '../Hal/HalModel';

export class ChildModel implements HalModel {
  id: number;
  firstName: string;
  lastName: string;
  parentId: number;
  gender: 'male' | 'female';
  age: number;
  pocketMoneyAmount: number;
  currency: string;
  balances: {
    spend: number;
    save: number;
    give: number;
  };

  constructor(childDAO: ChildData, parentId: number) {
    // Deep clone of childDAO
    const data = JSON.parse(JSON.stringify(childDAO));

    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.gender = data.gender;
    this.age = data.age;
    this.pocketMoneyAmount = data.pocketMoneyAmount;
    this.currency = data.currency;
    this.balances = data.balances;

    this.parentId = parentId;
  }

  toResource() {
    const resource = new hal.Resource(
      {
        id: this.id,
        firstName: this.firstName,
        lastName: this.lastName,
        gender: this.gender,
        age: this.age,
        pocketMoneyAmount: this.pocketMoneyAmount,
        currency: this.currency,
        balances: this.balances,
      },
      `/Rooster/parents/${this.parentId}/children/${this.id}`,
    );
    resource.link('parent', `/Rooster/parents/${this.parentId}`);

    return resource;
  }

  toHal() {
    return this.toResource().toJSON();
  }

  addPocketMoney(amount: number) {
    if (amount < 0) {
      throw new Error('Amount must be a postive value');
    }

    this.balances.spend += amount;
  }
}
