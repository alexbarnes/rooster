// @ts-ignore
import * as hal from 'hal';
import * as URI from 'urijs';
import * as hoek from 'hoek';

export class HalCollection {
  private readonly basePath: string;
  private readonly items: object[];
  private readonly start: number;
  private readonly count: number;
  private readonly total: number;
  private readonly embeddedType: string;

  constructor(
    basePath: string,
    items: object[],
    start: number,
    total: number,
    embeddedType: string,
  ) {
    this.basePath = basePath;
    this.items = items || [];
    this.start = start || 0;
    this.count = this.items.length;
    this.total = total || this.items.length || 0;
    this.embeddedType = embeddedType || '';
  }

  toHal() {
    const resource = new hal.Resource(
      {
        start: this.start,
        count: this.count,
        total: this.total,
      },
      this.basePath,
    );

    resource.embed(this.embeddedType, this.items, false);

    const uri = new URI(this.basePath);

    // TODO: this should come from request
    const limit = 10;

    if (this.total > limit) {
      const prev = Math.max(0, this.start - limit);
      const next = Math.min(this.total, this.start + limit);
      const last = this.total - limit;

      const query = uri.search(true);

      resource.link(
        'first',
        uri.search(hoek.applyToDefaults(query, { limit, start: 0 })).toString(),
      );

      if (this.start !== 0 && prev >= 0) {
        resource.link(
          'prev',
          uri
            .search(hoek.applyToDefaults(query, { limit, start: prev }))
            .toString()
        );
      }
      if (next < this.total) {
        resource.link(
          'next',
          uri
            .search(hoek.applyToDefaults(query, { limit, start: next }))
            .toString()
        );
      }
      if (last > 0) {
        resource.link(
          'last',
          uri
            .search(hoek.applyToDefaults(query, { limit, start: last }))
            .toString()
        );
      }
    }

    return resource.toJSON();
  }
}
