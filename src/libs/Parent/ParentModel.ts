// @ts-ignore
import * as hal from 'hal';
import { ParentData } from '../../data';
import { HalModel } from '../Hal/HalModel';
import { ChildModel } from '../Child/ChildModel';

export class ParentModel implements HalModel {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  childIds: number[];
  children: ChildModel[];

  constructor(parentDAO: ParentData, children?: ChildModel[]) {
    // Deep clone of parentDAO
    const data = JSON.parse(JSON.stringify(parentDAO));

    this.id = data.id;
    this.username = data.username;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.childIds = data.children;

    this.children = children || [];
  }

  toHal() {
    const resource = new hal.Resource(
      {
        id: this.id,
        username: this.username,
        firstName: this.firstName,
        lastName: this.lastName,
      },
      `/Rooster/parents/${this.id}`,
    );
    resource.link('children', `/Rooster/parents/${this.id}/children`);

    const childResources = this.children.map(child => child.toResource());
    resource.embed('children', childResources, false);

    return resource.toJSON();
  }

  shareMoney(amount: number): ChildModel[] {
    if (this.children.length === 0) {
      throw new Error('Parent has no children associated');
    }

    if (amount < 0) {
      throw new Error('Amount must be a postive value');
    }

    const ages = this.children.reduce((accumulator, child) => {
      return accumulator + child.age;
    }, 0);

    this.children.forEach(child => {
      const sharedAmount = Math.floor(amount * (child.age / ages));
      child.addPocketMoney(sharedAmount);
    });

    return this.children;
  }
}
