export interface ShareRequest {
  amount: number;
  currency: string;
}
