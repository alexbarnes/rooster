import { RoosterMoneyDataAccess } from '../../data';
import { ParentModel } from './ParentModel';
import { ChildModel } from '../Child/ChildModel';
import { ChildService } from '../Child/ChildService';

export interface ParentService {
  getParent(userId: number, parentId: number): Promise<ParentModel>;
  getChildren(userId: number, parentId: number): Promise<ChildModel[]>;
  shareMoney(
    userId: number,
    parentId: number,
    amount: number,
  ): Promise<object[]>;
}

export class ParentMockDataService implements ParentService {
  private readonly dataGateway: RoosterMoneyDataAccess;
  private readonly childService: ChildService;

  constructor(gateway: RoosterMoneyDataAccess, childService: ChildService) {
    this.dataGateway = gateway;
    this.childService = childService;
  }

  getParentData(userId: number, parentId: number) {
    if (userId !== parentId) {
      return Promise.reject('unauthorised');
    }
    return this.dataGateway.getParentData(parentId);
  }

  async getParent(userId: number, parentId: number) {
    const data = await this.getParentData(userId, parentId);

    const children = await this.getChildren(userId, parentId);

    return new ParentModel(data, children);
  }

  async getChildren(userId: number, parentId: number): Promise<ChildModel[]> {
    const parentData = await this.getParentData(userId, parentId);

    const proms: Promise<ChildModel>[] = parentData.children.map(childId =>
      this.childService.getChild(parentId, childId),
    );

    return Promise.all(proms);
  }

  async shareMoney(
    userId: number,
    parentId: number,
    amount: number,
  ): Promise<object[]> {
    const parent = await this.getParent(userId, parentId);

    parent.shareMoney(amount);

    // Update DataGateway
    const proms = parent.children.map(child => {
      return this.dataGateway.updateChildBalances(
        userId,
        child.id,
        child.balances,
      );
    });

    return Promise.all(proms);
  }
}
