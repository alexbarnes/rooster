swagger: '2.0'
info:
  version: "0.0.1"
  title: 'Rooster Node Challenge'
  description: 'An API to calculate balances and move money'
paths:
  /auth:
    post:
      tags:
        - Utils
      summary: "Authenticates a user"
      description: "Returns a Parent resource on successful authentication."
      parameters:
        - name: "body"
          in: "body"
          description: "Authentication credentials"
          required: true
          schema:
            $ref: "#/definitions/Credentials"
      responses:
        200:
          description: "Successful response"
          schema:
            $ref: "#/definitions/AuthToken"
        404:
          description: "User not found or password mismatch"
          schema:
            $ref: '#/definitions/ProblemJson'

  /parents/{id}:
    get:
      tags:
        - Banking
      summary: "Gets Parent resource"
      description: "Returns a Parent resource"
      parameters:
        - name: "id"
          in: "path"
          description: "UUID for Parent resource"
          required: true
          type: string
      responses:
        200:
          description: "Successful response"
          schema:
            $ref: "#/definitions/ParentResource"
        404:
          description: "Parent not found"
          schema:
            $ref: '#/definitions/ProblemJson'

  /parents/{parentId}/children:
    get:
      tags:
        - Banking
      summary: "Get all children of Parent"
      description: "Returns a Child collection for associated Parent."
      parameters:
        - name: "parentId"
          in: "path"
          description: "UUID for Parent resource"
          required: true
          type: string
      responses:
        200:
          description: "Successful response"
          schema:
            $ref: '#/definitions/Children'
        404:
          description: "Parent not found"
          schema:
            $ref: '#/definitions/ProblemJson'

  /parents/{parentId}/children/{childId}:
    get:
      tags:
        - Banking
      summary: "Get child resource"
      description: "Returns a Child resource for associated Parent."
      parameters:
        - name: "parentId"
          in: "path"
          description: "UUID for Parent resource"
          required: true
          type: string
        - name: "childId"
          in: "path"
          description: "UUID for Child resource"
          required: true
          type: string
      responses:
        200:
          description: "Successful response"
          schema:
            $ref: '#/definitions/ChildResource'
        404:
          description: "Parent not found"
          schema:
            $ref: '#/definitions/ProblemJson'

  /parents/{parentId}/share:
    post:
      tags:
        - Banking
      summary: "Share Money between children."
      description: "Shares an amount of money with all children, distributed based on the ratio of their age."
      parameters:
        - name: "parentId"
          in: "path"
          description: "UUID for Parent resource"
          required: true
          type: string
        - name: "body"
          in: "body"
          description: "Share Request"
          required: true
          schema:
            $ref: "#/definitions/ShareRequest"
      responses:
        204:
          description: "Successful response"
        404:
          description: "Parent not found"
          schema:
            $ref: '#/definitions/ProblemJson'
        409:
          description: "Unable to process request. For example parent may not have any associated children."
          schema:
            $ref: '#/definitions/ProblemJson'
        422:
          description: "Failed to parse amount"
          schema:
            $ref: '#/definitions/ProblemJson'

definitions:
  Resource:
    required:
    - _links
    type: object
    description: "A HAL resource."
    properties:
      _links:
        type: object
        properties:
          self:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources/identifier"

  Collection:
    required:
    - count
    - total
    - start
    - _links
    type: object
    description: "A collection of HAL resources."
    properties:
      _links:
        type: object
        properties:
          self:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources"
          first:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources?start=0&limit=10"
          prev:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources?start=80&limit=10"
          next:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources?start=100&limit=10"
          last:
            type: object
            properties:
              href:
                type: string
                example: "/Rooster/resources?start=190&limit=10"
      start:
        type: integer
        description: "Index of first resource in this request."
        example: 90
      count:
        type: integer
        description: "The count of the resources returned in this request."
        example: 10
      total:
        type: integer
        description: "Total number of resources."
        example: 200

  AuthToken:
    type: object
    properties:
      access_token:
        type: string
        description: "JSON Web Token (JWT) that can be used by an application to access the API."
      id_token:
        type: string
        description: "JSON Web Token (JWT) that contains user profile information."
      expires_in:
        type: integer
        description: "the number of seconds before the Access Token expires"
        example: 3600
        minimum: 60

  Credentials:
    type: object
    required:
      - username
      - password
    properties:
      username:
        type: string
        description: "Unique login identifier"
        example: "JonSmart@roostermoney.co.uk"
      password:
        type: string
        description: "Users password"
        example: "myP4ssw0rd"

  Parent:
    type: object
    required:
      - username
    properties:
      username:
        type: string
        description: "Unique login identifier"
        example: "JonSmart@roostermoney.co.uk"
      firstName:
        type: string
        description: "First or given name of parent"
        example: "Jon"
      lastName:
        type: string
        description: "Last or family name of parent"
        example: "Smart"

  ParentResource:
    description: "HAL representation of a Parent"
    allOf:
    - $ref: '#/definitions/Resource'
    - type: object
      properties:
        id:
          type: string
          description: UUID
          example: "123e4567-e89b-12d3-a456-426655440000"
    - $ref: '#/definitions/Parent'
    - type: object
      properties:
        _embedded:
          type: object
          properties:
            children:
              type: array
              items:
                $ref: "#/definitions/ChildResource"
              minItems: 0
              maxItems: 1000

  Child:
    type: object
    required:
      - age
      - balances
    properties:
      firstName:
        type: string
        description: "First or given name of child"
        example: "Kimi"
      lastName:
        type: string
        description: "Last or family name of child"
        example: "Machen"
      gender:
        type: string
        description: "Gender"
        example: "female"
      amount:
        type: number
        description: "Pocket Money Amount"
        example: 123.40
      currency:
        type: string
        description: "ISO 4217 Currency code"
        example: "gbp"
      balances:
        type: object
        properties:
          spend:
            type: number
            description: "Money available to spend"
            example: 123.40
          save:
            type: number
            description: "Money in savings"
            example: 123.40

  ChildResource:
    description: "HAL representation of an Item"
    allOf:
    - $ref: '#/definitions/Resource'
    - type: object
      properties:
        id:
          type: string
          description: UUID
          example: "123e4567-e89b-12d3-a456-426655440000"
    - $ref: '#/definitions/Child'

  Children:
    allOf:
    - $ref: '#/definitions/Collection'
    - type: object
      properties:
        _embedded:
          type: object
          properties:
            children:
              type: array
              items:
                $ref: "#/definitions/ChildResource"
              minItems: 0
              maxItems: 50

  ShareRequest:
    type: object
    required:
      - amount
    properties:
      amount:
        type: number
        description: "Pocket Money Amount"
        example: 123
        minimum: 0
      currency:
        type: string
        description: "ISO 4217 Currency code"
        example: "gbp"

  ProblemJson:
    type: object
    properties:
      detail:
        type: string
        example: "Failed Validation"
      status:
        type: number
        example: 422
      title:
        type: string
        example: "Unprocessable Entity"
      type:
        type: string
        example: "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html"
      validation_messages:
        type: array
        items:
          type: object

host: api.abarnes.uk
basePath: /Rooster
schemes:
- https
