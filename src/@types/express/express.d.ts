﻿import * as express from 'express';

declare global {
  namespace Express {
    // noinspection TsLint
    type Send<T> = (body?: T) => express.Response;
    interface TypedRequest<BODY> extends express.Request {
      body: BODY;
    }
    interface TypedResponse<BODY> extends express.Response {
      json: Send<BODY>;
      send: Send<BODY>;
    }
  }
}
