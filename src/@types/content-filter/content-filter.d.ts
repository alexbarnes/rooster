﻿declare module 'content-filter' {
  import express = require('express');
  function filter(options?: any): express.RequestHandler;
  namespace filter {

  }
  export = filter;
}
