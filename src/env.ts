const nodeEnv = process.env.NODE_ENV || 'development';

export default {
  nodeEnv,
  port: process.env.PORT,
  isLocal: nodeEnv === 'local',
  isDev: nodeEnv === 'development',
  isStaging: nodeEnv === 'staging',
  isProduction: nodeEnv === 'production',
};
