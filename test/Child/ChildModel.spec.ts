import 'mocha';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { ChildModel } from '../../src/libs/Child/ChildModel';
import { ChildData } from '../../src/data';

const expect = chai.expect;
chai.use(require('chai-http'));
chai.use(chaiAsPromised);

const mockChild: ChildData = {
  id: 301,
  firstName: 'Kimi',
  lastName: 'Machen',
  gender: 'female',
  age: 8,
  pocketMoneyAmount: 200,
  currency: 'gbp',
  balances: {
    spend: 2000,
    save: 1000,
    give: 0,
  },
};

describe('ChildModel Test', () => {
  it('should create new models', async () => {
    const model = new ChildModel(mockChild, 100);

    expect(model.id).to.equal(mockChild.id);
    expect(model.firstName).to.equal(mockChild.firstName);
    expect(model.lastName).to.equal(mockChild.lastName);
    expect(model.gender).to.equal(mockChild.gender);
    expect(model.pocketMoneyAmount).to.equal(mockChild.pocketMoneyAmount);
    expect(model.currency).to.equal(mockChild.currency);

    const { balances } = model;
    expect(balances.spend).to.equal(mockChild.balances.spend);
    expect(balances.save).to.equal(mockChild.balances.save);
    expect(balances.give).to.equal(mockChild.balances.give);

    expect(model.parentId).to.equal(100);
  });

  it('should render as HAL resource', async () => {
    const model = new ChildModel(mockChild, 100);
    const resource = model.toHal();

    expect(resource.id).to.equal(mockChild.id);
    expect(resource.firstName).to.equal(mockChild.firstName);
    expect(resource.lastName).to.equal(mockChild.lastName);

    expect(resource.gender).to.equal(mockChild.gender);
    expect(resource.pocketMoneyAmount).to.equal(mockChild.pocketMoneyAmount);
    expect(resource.currency).to.equal(mockChild.currency);

    const { balances } = resource;
    expect(balances.spend).to.equal(mockChild.balances.spend);
    expect(balances.save).to.equal(mockChild.balances.save);
    expect(balances.give).to.equal(mockChild.balances.give);

    expect(resource).to.not.have.property('parentId');

    expect(resource._links.self.href).to.equal(
      `/Rooster/parents/100/children/${mockChild.id}`,
    );

    expect(resource._links.parent.href).to.equal(
      `/Rooster/parents/100`,
    );
  });

  it('should allow additional pocket money', async () => {
    const model = new ChildModel(mockChild, 100);

    model.addPocketMoney(100);

    const { balances } = model;
    expect(balances.spend).to.equal(mockChild.balances.spend + 100);
    expect(balances.save).to.equal(mockChild.balances.save);
    expect(balances.give).to.equal(mockChild.balances.give);
  });

  it('should handle adding negative money', async () => {
    const model = new ChildModel(mockChild, 100);

    expect(() => {
      model.addPocketMoney(-100);
    }).to.throw('Amount must be a postive value');
  });
});
