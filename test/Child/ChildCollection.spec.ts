import 'mocha';
import * as chai from 'chai';
import { ChildCollection } from '../../src/libs/Child/ChildCollection';
import { ChildData, ParentData } from '../../src/data';
import { ChildModel } from '../../src/libs/Child/ChildModel';

const expect = chai.expect;
chai.use(require('chai-http'));

const mockParent: ParentData = {
  id: 100,
  username: 'JonSmart',
  password: 'Challenge',
  firstName: 'Jonathan',
  lastName: 'Smart',
  children: [301, 303, 304],
};

const mockChildData: ChildData = {
  id: 301,
  firstName: 'Kimi',
  lastName: 'Machen',
  gender: 'female',
  age: 8,
  pocketMoneyAmount: 200,
  currency: 'gbp',
  balances: {
    spend: 2000,
    save: 1000,
    give: 0,
  },
};

const mockChild: ChildModel = new ChildModel(mockChildData, mockParent.id);

describe('ChildCollection Test', () => {
  it('should create new collections', async () => {
    const collection = new ChildCollection([mockChild], mockParent.id);

    const hal = collection.toHal();
    expect(hal.start).to.equal(0);

    expect(hal.start).to.equal(0);
    expect(hal.total).to.equal(1);
    expect(hal.count).to.equal(1);

    const { children } = hal._embedded;

    expect(children[0].id).to.equal(mockChildData.id);

    expect(children[0]._links.self.href).to.equal(
      `/Rooster/parents/${mockParent.id}/children/${mockChildData.id}`,
    );
  });

});
