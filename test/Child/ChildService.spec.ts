import 'mocha';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { ChildMockDataService } from '../../src/libs/Child/ChildService';
import { ChildData, RoosterMoneyDataAccess } from '../../src/data';

const expect = chai.expect;
chai.use(require('chai-http'));
chai.use(chaiAsPromised);

const mockChild: ChildData = {
  id: 301,
  firstName: 'Kimi',
  lastName: 'Machen',
  gender: 'female',
  age: 8,
  pocketMoneyAmount: 200,
  currency: 'gbp',
  balances: {
    spend: 2000,
    save: 1000,
    give: 0,
  },
};

describe('ChildService Test', () => {
  it('should retreive ChildData - authorised', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.resolves(100);
    stubGateway.getChildData.resolves(mockChild);

    const service = new ChildMockDataService(stubGateway);

    const parent = await service.getChild(100, 301);

    expect(parent.id).to.equal(mockChild.id);
    expect(parent.firstName).to.equal(mockChild.firstName);
    expect(parent.lastName).to.equal(mockChild.lastName);

    expect(parent).to.not.have.property('password');
    expect(parent).to.not.have.property('token');
  });

  it('should handle gateway rejection', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.getChildData.rejects();

    const service = new ChildMockDataService(stubGateway);

    return expect(service.getChild(102, 333)).to.be.rejectedWith(
      'Error',
    );
  });
});
