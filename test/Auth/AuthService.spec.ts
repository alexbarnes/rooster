import 'mocha';
import * as chai from 'chai';
import * as sinon from 'sinon';
import { ParentAuthService } from '../../src/libs/Auth/AuthService';
import { ParentData, RoosterMoneyDataAccess } from '../../src/data';

const expect = chai.expect;
chai.use(require('chai-http'));

const mockParent: ParentData = {
  id: 100,
  username: 'JonSmart',
  password: 'Challenge',
  firstName: 'Jonathan',
  lastName: 'Smart',
  children: [301, 303, 304],
};

describe('AuthService Test', () => {
  it('should authenticate user', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.resolves(100);
    stubGateway.getParentData.resolves(mockParent);

    const service = new ParentAuthService(stubGateway);

    const auth = await service.authenticate(
      'JonSmart',
      'Challenge',
    );
    expect(auth.id).to.equal(100);
  });

  it('should authenticate user - password failure', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.rejects('Password failed');

    const service = new ParentAuthService(stubGateway);

    return service.authenticate('JonSmart', 'badPassword').catch(err => {
      expect(err.name).to.equal('Password failed');
    });
  });
});
