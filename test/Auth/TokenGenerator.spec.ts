import 'mocha';
import * as chai from 'chai';
import * as lolex from 'lolex';
import { JwtGenerator } from '../../src/libs/Auth/TokenGenerator';
import { ParentData } from '../../src/data';
import * as jwt from 'jsonwebtoken';

const expect = chai.expect;
chai.use(require('chai-http'));

let clock: lolex.Clock;

const mockParent: ParentData = {
  id: 100,
  username: 'JonSmart',
  password: 'Challenge',
  firstName: 'Jonathan',
  lastName: 'Smart',
  children: [301, 303, 304],
};

describe('TokenGenerator Test', () => {
  before(() => {
    clock = lolex.install({ now: 1462361249717, shouldAdvanceTime: true });
  });

  after(() => {
    clock.uninstall();
  });

  it('should generate valid id_token', () => {
    const generator = new JwtGenerator();
    const secret = 'shhh';

    const token = generator.createIdToken(mockParent, secret, 60);

    const decodedToken: any = jwt.verify(token, secret);

    expect(decodedToken.sub).to.equal(`${mockParent.id}`);
    expect(decodedToken.username).to.equal(mockParent.username);
    expect(decodedToken.iat).to.equal(1462361249);
    expect(decodedToken.exp).to.equal(1462361309);
    expect(decodedToken).to.not.have.property('children');
    expect(decodedToken).to.not.have.property('id');
    expect(decodedToken).to.not.have.property('password');
  });

  it('should generate valid access_token', () => {
    const generator = new JwtGenerator();
    const secret = 'shhh';

    const token = generator.createAccessToken('testSubject', secret, 60);

    const decodedToken: any = jwt.verify(token, secret);

    expect(decodedToken.sub).to.equal('testSubject');
    expect(decodedToken.iat).to.equal(1462361249);
    expect(decodedToken.exp).to.equal(1462361309);
  });
});
