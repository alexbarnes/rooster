import 'mocha';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
// @ts-ignore
import * as hal from 'hal';
import { HalCollection } from '../../src/libs/Hal/HalCollection';

const expect = chai.expect;
chai.use(require('chai-http'));
chai.use(chaiAsPromised);

describe('HalCollection Test', () => {
  it('should create new collection', async () => {
    const basePath = '/test';

    const order123 = new hal.Resource(
      {
        total: 30.0,
        currency: 'USD',
        status: 'shipped',
      },
      '/orders/123',
    );

    const order124 = new hal.Resource(
      {
        total: 20.0,
        currency: 'USD',
        status: 'processing',
      },
      '/orders/124',
    );

    const items = [order123, order124];

    const collection = new HalCollection(basePath, items, 10, 100, 'orders');

    const halCollection = collection.toHal();

    expect(halCollection.start).to.equal(10);
    expect(halCollection.total).to.equal(100);
    expect(halCollection.count).to.equal(items.length);

    expect(halCollection._embedded.orders.length).to.equal(2);
    expect(halCollection._embedded.orders[0].total).to.equal(order123.total);

    expect(halCollection._links.self.href).to.equal('/test');

    expect(halCollection._links.first.href).to.equal('/test?limit=10&start=0');
    expect(halCollection._links.prev.href).to.equal('/test?limit=10&start=0');
    expect(halCollection._links.next.href).to.equal('/test?limit=10&start=20');
    expect(halCollection._links.last.href).to.equal('/test?limit=10&start=90');
  });
});
