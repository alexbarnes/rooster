import 'mocha';
import * as chai from 'chai';
import server from '../src/server';

chai.should();
chai.use(require('chai-http'));

describe('Challenge Test', () => {
  after(done => {
    server.close(() => {
      console.info('HTTP Server Stopped');
      done();
    });
  });

  it('should respond ok to a root call', done => {
    chai
      .request(server)
      .get('/')
      .then(res => {
        res.should.have.status(200);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should return api docs', done => {
    chai
      .request(server)
      .get('/Rooster/api-docs')
      .then(res => {
        res.should.have.status(200);
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
