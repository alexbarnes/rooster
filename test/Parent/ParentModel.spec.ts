import 'mocha';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { ParentModel } from '../../src/libs/Parent/ParentModel';
import { ChildData, ParentData } from '../../src/data';
import { ChildModel } from '../../src/libs/Child/ChildModel';

const expect = chai.expect;
chai.use(require('chai-http'));
chai.use(chaiAsPromised);

const mockParent: ParentData = {
  id: 100,
  username: 'JonSmart',
  password: 'Challenge',
  firstName: 'Jonathan',
  lastName: 'Smart',
  children: [301, 303, 304],
  token: 'abd123',
};

const mockChildData: ChildData = {
  id: 301,
  firstName: 'Kimi',
  lastName: 'Machen',
  gender: 'female',
  age: 8,
  pocketMoneyAmount: 200,
  currency: 'gbp',
  balances: {
    spend: 2000,
    save: 1000,
    give: 0,
  },
};

const mockChild: ChildModel = new ChildModel(mockChildData, mockParent.id);

describe('ParentModel Test', () => {
  it('should create new models', async () => {
    const model = new ParentModel(mockParent);

    expect(model.id).to.equal(mockParent.id);
    expect(model.username).to.equal(mockParent.username);
    expect(model.firstName).to.equal(mockParent.firstName);
    expect(model.lastName).to.equal(mockParent.lastName);

    expect(model.childIds.length).to.equal(mockParent.children.length);
    expect(model.childIds[0]).to.equal(mockParent.children[0]);

    expect(model).to.not.have.property('password');
    expect(model).to.not.have.property('token');
  });

  it('should render as HAL resource', async () => {
    const model = new ParentModel(mockParent);
    const resource = model.toHal();

    expect(resource.id).to.equal(mockParent.id);
    expect(resource.username).to.equal(mockParent.username);
    expect(resource.firstName).to.equal(mockParent.firstName);
    expect(resource.lastName).to.equal(mockParent.lastName);

    expect(resource._embedded.children.length).to.equal(0);

    expect(resource).to.not.have.property('password');
    expect(resource).to.not.have.property('token');

    expect(resource._links.self.href).to.equal(
      `/Rooster/parents/${mockParent.id}`,
    );

    expect(resource._links.children.href).to.equal(
      `/Rooster/parents/${mockParent.id}/children`,
    );
  });

  it('should render as HAL resource - with children', async () => {
    const model = new ParentModel(mockParent, [mockChild]);
    const resource = model.toHal();

    const childrenHal = resource._embedded.children;
    expect(childrenHal.length).to.equal(1);
    expect(childrenHal[0].id).to.equal(mockChild.id);

    expect(childrenHal[0]._links.self.href).to.equal(
      `/Rooster/parents/${mockParent.id}/children/${mockChild.id}`,
    );
  });

  it('should share money with children', async () => {
    const mockChildData2 = JSON.parse(JSON.stringify(mockChildData));
    mockChildData2.age = 16;
    mockChildData2.balances.spend = 1500;

    const mockChild2: ChildModel = new ChildModel(
      mockChildData2,
      mockParent.id,
    );

    const model = new ParentModel(mockParent, [mockChild, mockChild2]);

    const prevSpend = mockChild.balances.spend;
    const prevSpend2 = mockChild2.balances.spend;

    model.shareMoney(1000);

    expect(mockChild.balances.spend).to.equal(prevSpend + 333);
    expect(mockChild2.balances.spend).to.equal(prevSpend2 + 666);
  });

  it('should share money with children - same age', async () => {
    const mockChildData2 = JSON.parse(JSON.stringify(mockChildData));
    mockChildData2.balances.spend = 1500;

    const mockChild2: ChildModel = new ChildModel(
      mockChildData2,
      mockParent.id,
    );

    const model = new ParentModel(mockParent, [mockChild, mockChild2]);

    const prevSpend = mockChild.balances.spend;
    const prevSpend2 = mockChild2.balances.spend;

    model.shareMoney(1000);

    expect(mockChild.balances.spend).to.equal(prevSpend + 500);
    expect(mockChild2.balances.spend).to.equal(prevSpend2 + 500);
  });

  it('should share money with children - single child', async () => {
    const model = new ParentModel(mockParent, [mockChild]);

    const prevSpend = mockChild.balances.spend;

    model.shareMoney(1000);

    expect(mockChild.balances.spend).to.equal(prevSpend + 1000);
  });

  it('should handle sharing zero money', async () => {
    const mockChildData2 = JSON.parse(JSON.stringify(mockChildData));
    mockChildData2.age = 16;
    mockChildData2.balances.spend = 1500;

    const mockChild2: ChildModel = new ChildModel(
      mockChildData2,
      mockParent.id,
    );

    const model = new ParentModel(mockParent, [mockChild, mockChild2]);

    const prevSpend = mockChild.balances.spend;
    const prevSpend2 = mockChild2.balances.spend;

    model.shareMoney(0);

    expect(mockChild.balances.spend).to.equal(prevSpend);
    expect(mockChild2.balances.spend).to.equal(prevSpend2);
  });

  it('should handle sharing negative money', async () => {
    const model = new ParentModel(mockParent, [mockChild]);

    expect(() => {
      model.shareMoney(-200);
    }).to.throw('Amount must be a postive value');
  });

  it('should handle sharing money when parent has no children', async () => {
    const model = new ParentModel(mockParent);

    expect(() => {
      model.shareMoney(1000);
    }).to.throw('Parent has no children associated');
  });
});
