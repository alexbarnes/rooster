import 'mocha';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { ParentMockDataService } from '../../src/libs/Parent/ParentService';
import { ChildData, ParentData, RoosterMoneyDataAccess } from '../../src/data';
import { ChildModel } from '../../src/libs/Child/ChildModel';
import { ChildMockDataService } from '../../src/libs/Child/ChildService';

const expect = chai.expect;
chai.use(require('chai-http'));
chai.use(chaiAsPromised);

const mockParent: ParentData = {
  id: 100,
  username: 'JonSmart',
  password: 'Challenge',
  firstName: 'Jonathan',
  lastName: 'Smart',
  children: [301, 303, 304],
};

const mockChildData: ChildData = {
  id: 301,
  firstName: 'Kimi',
  lastName: 'Machen',
  gender: 'female',
  age: 8,
  pocketMoneyAmount: 200,
  currency: 'gbp',
  balances: {
    spend: 2000,
    save: 1000,
    give: 0,
  },
};

const mockChild: ChildModel = new ChildModel(mockChildData, mockParent.id);

describe('ParentService Test', () => {
  it('should retrieve ParentModel - authorised', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.resolves(100);
    stubGateway.getParentData.resolves(mockParent);

    const stubChildService = <any>(
      sinon.createStubInstance(ChildMockDataService)
    );
    stubChildService.getChild.resolves(mockChild);

    const service = new ParentMockDataService(stubGateway, stubChildService);

    const parent = await service.getParent(100, 100);

    expect(parent.id).to.equal(mockParent.id);
    expect(parent.username).to.equal(mockParent.username);
    expect(parent.firstName).to.equal(mockParent.firstName);
    expect(parent.lastName).to.equal(mockParent.lastName);
    expect(parent.childIds.length).to.equal(mockParent.children.length);
    expect(parent.childIds[0]).to.equal(mockParent.children[0]);

    expect(parent).to.not.have.property('password');
    expect(parent).to.not.have.property('token');
  });

  it('should retrieve ParentModel - unauthorised', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.getParentData.resolves(mockParent);

    const stubChildService = <any>(
      sinon.createStubInstance(ChildMockDataService)
    );
    stubChildService.getChild.resolves(mockChild);

    const service = new ParentMockDataService(stubGateway, stubChildService);

    return expect(service.getParent(101, 100)).to.be.rejectedWith(
      'unauthorised',
    );
  });

  it('should retrieve ParentModel - not found', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.getParentData.rejects();

    const stubChildService = <any>(
      sinon.createStubInstance(ChildMockDataService)
    );
    stubChildService.getChild.resolves(mockChild);

    const service = new ParentMockDataService(stubGateway, stubChildService);

    return expect(service.getParent(102, 102)).to.be.rejectedWith('Error');
  });

  it('should retrieve Child collection - authorised', async () => {
    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.resolves(100);
    stubGateway.getParentData.resolves(mockParent);

    const stubChildService = <any>(
      sinon.createStubInstance(ChildMockDataService)
    );

    const mockChild2 = { ...mockChild, id: 303 };
    const mockChild3 = { ...mockChild, id: 304 };

    stubChildService.getChild.onCall(0).resolves(mockChild);
    stubChildService.getChild.onCall(1).resolves(mockChild2);
    stubChildService.getChild.onCall(2).resolves(mockChild3);

    const service = new ParentMockDataService(stubGateway, stubChildService);

    const children = await service.getChildren(100, 100);

    expect(children.length).to.equal(3);
    expect(children[0].id).to.equal(301);
    expect(children[1].id).to.equal(303);
    expect(children[2].id).to.equal(304);
  });

  it('should share money between all children', async () => {
    const mockBalance = {
      spend: 2000,
      save: 1000,
      give: 0,
    };

    const stubGateway = <any>sinon.createStubInstance(RoosterMoneyDataAccess);
    stubGateway.authenticateUser.resolves(100);
    stubGateway.getParentData.resolves(mockParent);
    stubGateway.updateChildBalances.resolves(mockBalance);

    const stubChildService = <any>(
      sinon.createStubInstance(ChildMockDataService)
    );
    stubChildService.getChild.resolves(mockChild);

    const service = new ParentMockDataService(stubGateway, stubChildService);

    const balances = await service.shareMoney(100, 100, 1000);

    expect(balances.length).to.equal(3);
    expect(balances[0]).to.equal(mockBalance);
  });
});
