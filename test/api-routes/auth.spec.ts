import 'mocha';
import * as chai from 'chai';
import * as jwt from 'jsonwebtoken';
import * as lolex from 'lolex';
import server from '../../src/server';

const expect = chai.expect;
chai.should();
chai.use(require('chai-http'));

let clock: lolex.Clock;

describe('/auth operation', () => {
  before(() => {
    clock = lolex.install({ now: 1462361249717, shouldAdvanceTime: true });
  });

  after(done => {
    clock.uninstall();
    server.close(() => {
      console.info('HTTP Server Stopped');
      done();
    });
  });

  it('should Authenticate - success', done => {
    chai
      .request(server)
      .post('/Rooster/auth.ts')
      .send({
        username: 'JonSmart',
        password: 'Challenge',
      })
      .then(res => {
        res.should.have.status(200);
        res.body.should.have.property('access_token');
        res.body.should.have.property('id_token');
        res.body.should.have.property('expires_in');
        res.body.should.not.have.property('password');

        expect(res.body.expires_in).to.equal(3600);

        const decodedAccessToken: any = jwt.verify(
          res.body.access_token,
          'shhhh',
        );

        expect(decodedAccessToken.sub).to.equal('100');
        expect(decodedAccessToken.iat).to.equal(1462361249);
        expect(decodedAccessToken.exp).to.equal(1462364849);

        const decodedIdToken: any = jwt.verify(res.body.id_token, 'shhhh');

        expect(decodedIdToken.sub).to.equal('100');
        expect(decodedIdToken.username).to.equal('JonSmart');
        expect(decodedIdToken.iat).to.equal(1462361249);
        expect(decodedIdToken.exp).to.equal(1462364849);

        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should Authenticate - unauthorised password mismatch', done => {
    chai
      .request(server)
      .post('/Rooster/auth.ts')
      .send({
        username: 'JonSmart',
        password: 'BadPassword',
      })
      .then(res => {
        res.should.have.status(401);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should Authenticate - unauthorised username not found', done => {
    chai
      .request(server)
      .post('/Rooster/auth.ts')
      .send({
        username: 'UnknownUser',
        password: 'Challenge',
      })
      .then(res => {
        res.should.have.status(401);
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
