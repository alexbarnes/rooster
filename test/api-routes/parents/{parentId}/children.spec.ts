import 'mocha';
import * as chai from 'chai';
import server from '../../../../src/server';
import { JwtGenerator } from '../../../../src/libs/Auth/TokenGenerator';

const expect = chai.expect;
chai.should();
chai.use(require('chai-http'));

const generator = new JwtGenerator();

describe('/parent/{parentId}/children operation', () => {
  after(done => {
    server.close(() => {
      console.info('HTTP Server Stopped');
      done();
    });
  });

  it('should return child collection', done => {
    const accessToken = generator.createAccessToken('100', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/100/children.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(200);

        expect(res.body.start).to.equal(0);
        expect(res.body.total).to.equal(3);
        expect(res.body.count).to.equal(3);

        const { children } = res.body._embedded;

        expect(children[0].id).to.equal(301);
        expect(children[1].id).to.equal(303);
        expect(children[2].id).to.equal(304);

        expect(children[0]._links.self.href).to.equal(
          '/Rooster/parents/100/children/301',
        );

        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should return handle parent not found', done => {
    const accessToken = generator.createAccessToken('200', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/200/children.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should authorise user', done => {
    const accessToken = generator.createAccessToken('102', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/100/children.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
