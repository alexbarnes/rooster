import 'mocha';
import * as chai from 'chai';
import server from '../../../../../src/server';
import { JwtGenerator } from '../../../../../src/libs/Auth/TokenGenerator';

chai.should();
chai.use(require('chai-http'));

const generator = new JwtGenerator();

describe('/parents/{parentId}/children/{childId} operation', () => {
  after(done => {
    server.close(() => {
      console.info('HTTP Server Stopped');
      done();
    });
  });

  it('should return child resource', done => {
    const accessToken = generator.createAccessToken('100', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/100/children/301.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(200);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should return handle child not found', done => {
    const accessToken = generator.createAccessToken('200', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/100/children/111.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should authorise user', done => {
    const accessToken = generator.createAccessToken('102', 'shhhh', 60);

    chai
      .request(server)
      .get('/Rooster/parents/100/children/301.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
