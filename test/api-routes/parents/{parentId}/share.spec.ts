import 'mocha';
import * as chai from 'chai';
import server from '../../../../src/server';
import { JwtGenerator } from '../../../../src/libs/Auth/TokenGenerator';
import { ParentMockDataService } from '../../../../src/libs/Parent/ParentService';
import RoosterMoneyDataAccess from '../../../../src/data';
import { ChildMockDataService } from '../../../../src/libs/Child/ChildService';
import { ShareRequest } from '../../../../src/libs/Parent/ShareRequest';

const expect = chai.expect;
chai.should();
chai.use(require('chai-http'));

const generator = new JwtGenerator();

describe('/parent/{parentId}/share operation', () => {
  after(done => {
    server.close(() => {
      console.info('HTTP Server Stopped');
      done();
    });
  });

  it('should update child balances', async () => {
    const accessToken = generator.createAccessToken('100', 'shhhh', 60);

    const shareRequest: ShareRequest = {
      amount: 1000,
      currency: 'gbp',
    };

    const shareResp = await chai
      .request(server)
      .post('/Rooster/parents/100/share.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(shareRequest);

    expect(shareResp).to.have.status(204);

    const childService = new ChildMockDataService(RoosterMoneyDataAccess);
    const parentService = new ParentMockDataService(
      RoosterMoneyDataAccess,
      childService,
    );

    const children = await parentService.getChildren(100, 100);

    expect(children[0].balances.spend).to.equal(2307);
    expect(children[1].balances.spend).to.equal(2384);
    expect(children[2].balances.spend).to.equal(2307);
  });

  it('should return handle parent not found', done => {
    const accessToken = generator.createAccessToken('200', 'shhhh', 60);

    chai
      .request(server)
      .post('/Rooster/parents/200/share.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should authorise user', done => {
    const accessToken = generator.createAccessToken('102', 'shhhh', 60);

    chai
      .request(server)
      .post('/Rooster/parents/100/children.ts')
      .set('Authorization', `Bearer ${accessToken}`)
      .then(res => {
        res.should.have.status(404);
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
