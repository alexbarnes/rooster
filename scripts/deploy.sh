#!/usr/bin/env bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -s|--stackname)
    STACK_POSTFIX="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

set -e

[ -z "$AWS_ACCOUNT_ID" ] && echo "Need to set AWS_ACCOUNT_ID" && exit 1;
[ -z "$AWS_DEFAULT_REGION" ] && echo "Need to set AWS_DEFAULT_REGION" && exit 1;

if [[ $STACK_POSTFIX == "prod" ]];
then
    echo "Setting Production env vars"
    eval "`./node_modules/.bin/aws-assume prod`"

    export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    export AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN

    export AWS_ACCOUNT_ID=xxxxxxxxxx
    PROFILE_FLAG="--stage prod"
fi

echo "npx serverless deploy --verbose ${PROFILE_FLAG}"
npx serverless deploy --verbose ${PROFILE_FLAG}
