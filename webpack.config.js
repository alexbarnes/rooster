const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');
const entry = require('webpack-glob-entry');

const isDebug = true;
const isServerless =
  slsw.lib.webpack.isLocal || !!slsw.lib.entries['src/lambda'];

const reTypeScript = /\.(ts)$/;

let entries;
if (isServerless) {
  const globEntries = entry(entry.basePath('./'), './src/api-routes/**/*.ts');

  entries = { ...globEntries, ...slsw.lib.entries };
} else {
  entries = entry(
    entry.basePath('./src'),
    './src/server.ts',
    './src/api-routes/**/*.ts',
  );
}

const assetPrefix = isServerless ? './src/' : './';

module.exports = {
  devtool: 'inline-source-map',
  entry: entries,
  target: 'node',
  externals: [nodeExternals()],
  output: {
    libraryTarget: 'commonjs',
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },

  mode: isDebug ? 'development' : 'production',

  module: {
    rules: [
      {
        test: reTypeScript,
        loaders: ['ts-loader'],
      },
    ],
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: './src/config', to: `${assetPrefix}/config/` },
    ]),
  ],

  resolve: {
    extensions: ['.ts', '.js', '.json'],
    // Allow absolute paths in imports, e.g. import Button from 'components/Button'
    modules: ['node_modules', 'src'],
  },

  // Do not replace node globals with polyfills
  // https://webpack.js.org/configuration/node/
  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  },
};
